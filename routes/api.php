<?php

use Illuminate\Http\Request;


Route::post('auth/login', 'Api\AuthController@login');
Route::post('auth/register', 'Api\AuthController@register');
Route::get('courier/profile', 'Api\AuthController@profile')->middleware('auth:api');
Route::get('packages', 'Api\PackageController@packages');
Route::get('users', 'UserController@users');
