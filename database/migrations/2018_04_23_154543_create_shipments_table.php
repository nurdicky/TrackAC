<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shipment_shipper');
            $table->unsignedInteger('shipment_consignee');
            $table->unsignedInteger('shipment_courier');
            $table->string('shipment_origin');
            $table->string('shipment_destination');
            $table->dateTime('shipment_date');
            $table->string('shipment_expected_delivery');
            $table->unsignedInteger('shipment_service');
            $table->integer('shipment_package_count');
            $table->timestamps();

            $table->foreign('shipment_shipper')->references('id')->on('customers');
            $table->foreign('shipment_courier')->references('id')->on('couriers');
            $table->foreign('shipment_consigne')->references('id')->on('customers');
            $table->foreign('shipment_service')->references('id')->on('shipment_services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
}
