<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentChekpointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipment_chekpoints', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('package_id');
            $table->string('checkpoint_time');
            $table->string('checkpoint_cordinate');
            $table->unsignedInteger('checkpoint_status');
            $table->timestamps();

            $table->foreign('package_id')->references('id')->on('packages');
            $table->foreign('checkpoint_status')->references('id')->on('shipment_statuses');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipment_chekpoints');
    }
}
