<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Courier::class, function (Faker $faker) {
    return [
        'courier_name' => $faker->name,
        'courier_address' => $faker->address,
        'courier_phone' => $faker->phoneNumber,
        'courier_number' => rand(),
        'courier_password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
    ];
});
