<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Shipment::class, function (Faker $faker) {
    return [
      'shipment_shipper' => function () {
        return App\Models\Customer::find($customer['customer_type'])->id;
      },
      'shipment_consagne' => function (array $customer) {
        return App\Models\Customer::find($customer['customer_type'])->id;
      }
      'shipment_origin' => $faker->title,
      'shipment_destination' => $faker->paragraph,
      'shipment_date' => $faker->paragraph,
      'shipment_expected_delivery' => $faker->paragraph,
      'shipment_service' => $faker->paragraph,
      'shipment_package_count' => $faker->paragraph,
    ];
});
