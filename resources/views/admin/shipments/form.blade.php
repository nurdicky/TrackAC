@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Add Data Shipment
                	<a style="float: right;" class="btn btn-primary" href="{{ route('shipment.index') }}">Back</a>
            	</div>

                <div class="panel-body">

                    @if(@$shipments->exists) <?php  $url = route('shipment.update', @$shipments->id); ?>
                    @else <?php  $url = route('shipment.store'); ?>
                    @endif
                	<form class="form-horizontal" action="{{ $url }}" method="POST">

                        @if (@$shipments->exists)
                            {{ method_field('PUT') }}
                        @else
                            {{ method_field('POST') }}
                        @endif

                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('shipment_shipper') ? ' has-error' : '' }}">
                            <label for="shipment_shipper" class="col-md-4 control-label">Shipper</label>

                            <div class="col-md-6">
                                <input id="shipment_shipper" type="text" class="form-control" name="shipment_shipper" value="{{ @$shipments->shipment_shipper }}"required>

                                @if ($errors->has('shipment_shipper'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shipment_shipper') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('shipment_consignee') ? ' has-error' : '' }}">
                            <label for="shipment_consignee" class="col-md-4 control-label">Consignee</label>

                            <div class="col-md-6">
                                <input id="shipment_consignee" type="text" class="form-control" name="shipment_consignee" value="{{ @$shipments->shipment_consignee }}"required>

                                @if ($errors->has('shipment_consignee'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shipment_consignee') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('shipment_courier') ? ' has-error' : '' }}">
                            <label for="shipment_courier" class="col-md-4 control-label">Courier</label>

                            <div class="col-md-6">
                                <input id="shipment_courier" type="text" class="form-control" name="shipment_courier" value="{{ @$shipments->shipment_courier }}"required>

                                @if ($errors->has('shipment_courier'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shipment_courier') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('shipment_origin') ? ' has-error' : '' }}">
                            <label for="shipment_origin" class="col-md-4 control-label">Origin</label>

                            <div class="col-md-6">
                                <input id="shipment_origin" type="text" class="form-control" name="shipment_origin" value="{{ @$shipments->shipment_origin }}"required>

                                @if ($errors->has('shipment_origin'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shipment_origin') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('shipment_destination') ? ' has-error' : '' }}">
                            <label for="shipment_destination" class="col-md-4 control-label">Destination</label>

                            <div class="col-md-6">
                                <input id="shipment_destination" type="text" class="form-control" name="shipment_destination" value="{{ @$shipments->shipment_destination }}"required>

                                @if ($errors->has('shipment_destination'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shipment_destination') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('shipment_date') ? ' has-error' : '' }}">
                            <label for="shipment_date" class="col-md-4 control-label">Shipment Date</label>

                            <div class="col-md-6">
                                <input id="shipment_date" type="text" class="form-control" name="shipment_date" value="{{ @$shipments->shipment_date }}"required>

                                @if ($errors->has('shipment_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shipment_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('shipment_expected_delivery') ? ' has-error' : '' }}">
                            <label for="shipment_expected_delivery" class="col-md-4 control-label">Expected Delivery</label>

                            <div class="col-md-6">
                                <input id="shipment_expected_delivery" type="text" class="form-control" name="shipment_expected_delivery" value="{{ @$shipments->shipment_expected_delivery }}"required>

                                @if ($errors->has('shipment_expected_delivery'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shipment_expected_delivery') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('shipment_service') ? ' has-error' : '' }}">
                            <label for="shipment_service" class="col-md-4 control-label">Service</label>

                            <div class="col-md-6">
                                <input id="shipment_service" type="text" class="form-control" name="shipment_service" value="{{ @$shipments->shipment_service }}"required>

                                @if ($errors->has('shipment_service'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shipment_service') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('shipment_package_count') ? ' has-error' : '' }}">
                            <label for="shipment_package_count" class="col-md-4 control-label">Package Count</label>

                            <div class="col-md-6">
                                <input id="shipment_package_count" type="text" class="form-control" name="shipment_package_count" value="{{ @$shipments->shipment_package_count }}"required>

                                @if ($errors->has('shipment_package_count'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shipment_package_count') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @if (@$shipments->exists)
                                        Update Shipment
                                    @else
                                        Add Shipment
                                    @endif
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
