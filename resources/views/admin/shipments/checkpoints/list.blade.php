@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Data Shipment Checkpoint
                	<a style="float: right;" class="btn btn-primary" href="{{ route('checkpoint.create') }}">Add</a>
            	</div>

                <div class="panel-body">

                	@if(Session::has('alert-success'))
		                <div class="alert alert-success">
		                    <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
		                </div>
  		            @endif

                	<table class="table table-stripped table-responsive">
                		<thead>
                			<th>#</th>
                			<th>Package id</th>
                			<th>Checkpoint time</th>
                			<th>Checkpoint cordinate</th>
                			<th>Checkpoint status</th>
                			<th></th>
                		</thead>
                		<tbody>
                			@php $no = 1; @endphp
                			@foreach ($checkpoints as $checkpoint)
        								<tr>
        									<td>{{ $no++ }}</td>
        									<td>{{ $checkpoint->package_id }}</td>
        									<td>{{ $checkpoint->checkpoint_time }}</td>
        									<td>{{ $checkpoint->checkpoint_cordinate }}</td>
        									<td>{{ $checkpoint->checkpoint_status }}</td>
        									<td>
        										<form action="{{ route('checkpoint.destroy', $checkpoint->id) }}" method="post">
                              {{ csrf_field() }}
                              {{ method_field('DELETE') }}
                              <a class="btn btn-sm btn-info" href="{{ route('checkpoint.edit', $checkpoint->id) }}">Edit</a>
                              <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                      			</form>
                    			</td>
        								</tr>
        							@endforeach
                		</tbody>
                	</table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
