@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Add Data Shipment Checkpoint
                	<a style="float: right;" class="btn btn-primary" href="{{ route('checkpoint.index') }}">Back</a>
            	</div>

                <div class="panel-body">

                    @if(@$checkpoints->exists) <?php  $url = route('checkpoint.update', @$checkpoints->id); ?>
                    @else <?php  $url = route('checkpoint.store'); ?>
                    @endif
                	<form class="form-horizontal" action="{{ $url }}" method="POST">

                        @if (@$checkpoints->exists)
                            {{ method_field('PUT') }}
                        @else
                            {{ method_field('POST') }}
                        @endif

                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('package_id') ? ' has-error' : '' }}">
                            <label for="package_id" class="col-md-4 control-label">Package</label>

                            <div class="col-md-6">
                                <input id="package_id" type="text" class="form-control" name="package_id" value="{{ @$checkpoints->package_id }}"required>

                                @if ($errors->has('package_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('package_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('checkpoint_time') ? ' has-error' : '' }}">
                            <label for="checkpoint_time" class="col-md-4 control-label">Time</label>

                            <div class="col-md-6">
                                <input id="checkpoint_time" type="text" class="form-control" name="checkpoint_time" value="{{ @$checkpoints->checkpoint_time }}"required>

                                @if ($errors->has('checkpoint_time'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('checkpoint_time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('checkpoint_cordinate') ? ' has-error' : '' }}">
                            <label for="checkpoint_cordinate" class="col-md-4 control-label">Cordinate</label>

                            <div class="col-md-6">
                                <input id="checkpoint_cordinate" type="text" class="form-control" name="checkpoint_cordinate" value="{{ @$checkpoints->checkpoint_cordinate }}"required>

                                @if ($errors->has('checkpoint_cordinate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('checkpoint_cordinate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('checkpoint_status') ? ' has-error' : '' }}">
                            <label for="checkpoint_status" class="col-md-4 control-label">Status</label>

                            <div class="col-md-6">
                                <input id="checkpoint_status" type="text" class="form-control" name="checkpoint_status" value="{{ @$checkpoints->checkpoint_status }}"required>

                                @if ($errors->has('checkpoint_status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('checkpoint_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button checkpoint="submit" class="btn btn-primary">
                                    @if (@$checkpoints->exists)
                                        Update Checkpoint
                                    @else
                                        Add Checkpoint
                                    @endif
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
