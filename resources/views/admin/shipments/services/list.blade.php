@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Data Shipment Service
                	<a style="float: right;" class="btn btn-primary" href="{{ route('service.create') }}">Add</a>
            	</div>

                <div class="panel-body">

                	@if(Session::has('alert-success'))
		                <div class="alert alert-success">
		                    <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
		                </div>
  		            @endif

                	<table class="table table-stripped table-responsive">
                		<thead>
                			<th>#</th>
                			<th>Service name</th>
                			<th>Service type</th>
                			<th></th>
                		</thead>
                		<tbody>
                			@php $no = 1; @endphp
                			@foreach ($services as $service)
        								<tr>
        									<td>{{ $no++ }}</td>
        									<td>{{ $service->service_name }}</td>
        									<td>{{ $service->service_type }}</td>
        									<td>
        										<form action="{{ route('service.destroy', $service->id) }}" method="post">
                              {{ csrf_field() }}
                              {{ method_field('DELETE') }}
                              <a class="btn btn-sm btn-info" href="{{ route('service.edit', $service->id) }}">Edit</a>
                              <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                      			</form>
                    			</td>
        								</tr>
        							@endforeach
                		</tbody>
                	</table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
