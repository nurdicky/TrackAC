@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Add Data Shipment Service
                	<a style="float: right;" class="btn btn-primary" href="{{ route('service.index') }}">Back</a>
            	</div>

                <div class="panel-body">

                    @if(@$services->exists) <?php  $url = route('service.update', @$services->id); ?>
                    @else <?php  $url = route('service.store'); ?>
                    @endif
                	<form class="form-horizontal" action="{{ $url }}" method="POST">

                        @if (@$services->exists)
                            {{ method_field('PUT') }}
                        @else
                            {{ method_field('POST') }}
                        @endif

                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('service_name') ? ' has-error' : '' }}">
                            <label for="service_name" class="col-md-4 control-label">Service Name</label>

                            <div class="col-md-6">
                                <input id="service_name" type="text" class="form-control" name="service_name" value="{{ @$services->service_name }}"required>

                                @if ($errors->has('service_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('service_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('service_type') ? ' has-error' : '' }}">
                            <label for="service_type" class="col-md-4 control-label">Service Type</label>

                            <div class="col-md-6">
                                <input id="service_type" type="text" class="form-control" name="service_type" value="{{ @$services->service_type }}"required>

                                @if ($errors->has('service_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('service_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button service="submit" class="btn btn-primary">
                                    @if (@$services->exists)
                                        Update Service
                                    @else
                                        Add Service
                                    @endif
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
