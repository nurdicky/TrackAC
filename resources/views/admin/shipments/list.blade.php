@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Data Shipment
                	<a style="float: right;" class="btn btn-primary" href="{{ route('shipment.create') }}">Add</a>
            	</div>

                <div class="panel-body">

                	@if(Session::has('alert-success'))
		                <div class="alert alert-success">
		                    <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
		                </div>
		            @endif

                	<table class="table table-stripped table-responsive">
                		<thead>
                			<th>#</th>
                			<th>Shipper</th>
                			<th>Consignee</th>
                			<th>Courier</th>
                			<th>Origin</th>
                			<th>Destination</th>
                			<th>Date</th>
                			<th>Expected Delivery</th>
                			<th>Service</th>
                			<th>Package Count</th>
                			<th></th>
                		</thead>
                		<tbody>
                			@php $no = 1; @endphp
                			@foreach ($shipments as $shipment)
        								<tr>
        									<td>{{ $no++ }}</td>
        									<td>{{ $shipment->shippers->customer_name }}</td>
        									<td>{{ $shipment->consignees->customer_name }}</td>
        									<td>{{ $shipment->couriers->courier_name }}</td>
        									<td>{{ $shipment->shipment_origin }}</td>
        									<td>{{ $shipment->shipment_destination }}</td>
        									<td>{{ $shipment->shipment_date }}</td>
        									<td>{{ $shipment->shipment_expected_delivery }}</td>
        									<td>{{ $shipment->services->service_name }}</td>
        									<td>{{ $shipment->shipment_package_count }}</td>
        									<td>
        										<form action="{{ route('shipment.destroy', $shipment->id) }}" method="post">
                              {{ csrf_field() }}
                              {{ method_field('DELETE') }}
                              <a class="btn btn-sm btn-info" href="{{ route('shipment.edit', $shipment->id) }}">Edit</a>
                              <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                      			</form>
                    			</td>
        								</tr>
        							@endforeach
                		</tbody>
                	</table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
