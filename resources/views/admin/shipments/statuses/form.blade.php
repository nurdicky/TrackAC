@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Add Data Shipment Status
                	<a style="float: right;" class="btn btn-primary" href="{{ route('status.index') }}">Back</a>
            	</div>

                <div class="panel-body">

                    @if(@$statuses->exists) <?php  $url = route('status.update', @$statuses->id); ?>
                    @else <?php  $url = route('status.store'); ?>
                    @endif
                	<form class="form-horizontal" action="{{ $url }}" method="POST">

                        @if (@$statuses->exists)
                            {{ method_field('PUT') }}
                        @else
                            {{ method_field('POST') }}
                        @endif

                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('status_tag') ? ' has-error' : '' }}">
                            <label for="status_tag" class="col-md-4 control-label">Status Tag</label>

                            <div class="col-md-6">
                                <input id="status_tag" type="text" class="form-control" name="status_tag" value="{{ @$statuses->status_tag }}"required>

                                @if ($errors->has('status_tag'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status_tag') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('status_message') ? ' has-error' : '' }}">
                            <label for="status_message" class="col-md-4 control-label">Status Message</label>

                            <div class="col-md-6">
                                <input id="status_message" type="text" class="form-control" name="status_message" value="{{ @$statuses->status_message }}"required>

                                @if ($errors->has('status_message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status_message') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @if (@$statuses->exists)
                                        Update Status
                                    @else
                                        Add Status
                                    @endif
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
