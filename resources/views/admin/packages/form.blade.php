@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Add Data Package
                	<a style="float: right;" class="btn btn-primary" href="{{ route('package.index') }}">Back</a>
            	</div>

                <div class="panel-body">

                    @if(@$packages->exists) <?php  $url = route('package.update', @$packages->id); ?>
                    @else <?php  $url = route('package.store'); ?>
                    @endif
                	<form class="form-horizontal" action="{{ $url }}" method="POST">

                        @if (@$packages->exists)
                            {{ method_field('PUT') }}
                        @else
                            {{ method_field('POST') }}
                        @endif

                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('package_number') ? ' has-error' : '' }}">
                            <label for="package_number" class="col-md-4 control-label">Package Number</label>

                            <div class="col-md-6">
                                <input id="package_number" type="text" class="form-control" name="package_number" value="{{ @$packages->package_number }}"required>

                                @if ($errors->has('package_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('package_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('package_size') ? ' has-error' : '' }}">
                            <label for="package_size" class="col-md-4 control-label">Package Size</label>

                            <div class="col-md-6">
                                <input id="package_size" type="text" class="form-control" name="package_size" value="{{ @$packages->package_size }}"required>

                                @if ($errors->has('package_size'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('package_size') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('package_weight') ? ' has-error' : '' }}">
                            <label for="package_weight" class="col-md-4 control-label">Package Weight</label>

                            <div class="col-md-6">
                                <input id="package_weight" type="text" class="form-control" name="package_weight" value="{{ @$packages->package_weight }}"required>

                                @if ($errors->has('package_weight'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('package_weight') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('package_weight_unit') ? ' has-error' : '' }}">
                            <label for="package_weight_unit" class="col-md-4 control-label">Package Weight / Unit</label>

                            <div class="col-md-6">
                                <input id="package_weight_unit" type="text" class="form-control" name="package_weight_unit" value="{{ @$packages->package_weight_unit }}"required>

                                @if ($errors->has('package_weight_unit'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('package_weight_unit') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('package_content') ? ' has-error' : '' }}">
                            <label for="package_content" class="col-md-4 control-label">Package Content</label>

                            <div class="col-md-6">
                                <input id="package_content" type="text" class="form-control" name="package_content" value="{{ @$packages->package_content }}"required>

                                @if ($errors->has('package_content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('package_content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('package_information') ? ' has-error' : '' }}">
                            <label for="package_information" class="col-md-4 control-label">Package Information</label>

                            <div class="col-md-6">
                                <input id="package_information" type="text" class="form-control" name="package_information" value="{{ @$packages->package_information }}"required>

                                @if ($errors->has('package_information'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('package_information') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @if (@$packages->exists)
                                        Update Package
                                    @else
                                        Add Package
                                    @endif
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
