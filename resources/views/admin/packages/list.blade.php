@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Data Package
                	<a style="float: right;" class="btn btn-primary" href="{{ route('package.create') }}">Add</a>
            	</div>

                <div class="panel-body">

                	@if(Session::has('alert-success'))
		                <div class="alert alert-success">
		                    <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
		                </div>
		            @endif

                	<table class="table table-stripped table-responsive">
                		<thead>
                			<th>#</th>
                			<th>Number</th>
                			<th>Size</th>
                			<th>Weight</th>
                			<th>Weight / size</th>
                			<th>Content</th>
                			<th>Information</th>
                			<th></th>
                		</thead>
                		<tbody>
                			@php $no = 1; @endphp
                			@foreach ($packages as $package)
        								<tr>
        									<td>{{ $no++ }}</td>
        									<td>{{ $package->package_number }}</td>
        									<td>{{ $package->package_size }}</td>
        									<td>{{ $package->package_weight }}</td>
        									<td>{{ $package->package_weight_unit }}</td>
        									<td>{{ $package->package_content }}</td>
        									<td>{{ $package->package_information }}</td>
        									<td>
        										<form action="{{ route('package.destroy', $package->id) }}" method="post">
                              {{ csrf_field() }}
                              {{ method_field('DELETE') }}
                              <a class="btn btn-sm btn-info" href="{{ route('package.edit', $package->id) }}">Edit</a>
                              <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                      			</form>
                    			</td>
        								</tr>
        							@endforeach
                		</tbody>
                	</table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
