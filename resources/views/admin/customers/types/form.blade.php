@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Add Data Customer Types
                	<a style="float: right;" class="btn btn-primary" href="{{ route('type.index') }}">Back</a>
            	</div>

                <div class="panel-body">

                    @if(@$types->exists) <?php  $url = route('type.update', @$types->id); ?>
                    @else <?php  $url = route('type.store'); ?>
                    @endif
                	<form class="form-horizontal" action="{{ $url }}" method="POST">

                        @if (@$types->exists)
                            {{ method_field('PUT') }}
                        @else
                            {{ method_field('POST') }}
                        @endif

                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('customer_type_name') ? ' has-error' : '' }}">
                            <label for="customer_type_name" class="col-md-4 control-label">Customer Type Name</label>

                            <div class="col-md-6">
                                <input id="customer_type_name" type="text" class="form-control" name="customer_type_name" value="{{ @$customers->customer_type_name }}"required>

                                @if ($errors->has('customer_type_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_type_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @if (@$types->exists)
                                        Update Types
                                    @else
                                        Add Types
                                    @endif
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
