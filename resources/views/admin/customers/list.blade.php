@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Data Customer
                	<a style="float: right;" class="btn btn-primary" href="{{ route('customer.create') }}">Add</a>
            	</div>

                <div class="panel-body">

                	@if(Session::has('alert-success'))
		                <div class="alert alert-success">
		                    <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
		                </div>
		            @endif

                	<table class="table table-stripped table-responsive">
                		<thead>
                			<th>#</th>
                			<th>Name</th>
                			<th>Address</th>
                			<th>Phone</th>
                			<th>Customer type</th>
                			<th></th>
                		</thead>
                		<tbody>
                			@php $no = 1; @endphp
                			@foreach ($customers as $customer)
        								<tr>
        									<td>{{ $no++ }}</td>
        									<td>{{ $customer->customer_name }}</td>
        									<td>{{ $customer->customer_address }}</td>
        									<td>{{ $customer->customer_phone }}</td>
        									<td>{{ $customer->types->customer_type_name }}</td>
        									<td>
        										<form action="{{ route('customer.destroy', $customer->id) }}" method="post">
                              {{ csrf_field() }}
                              {{ method_field('DELETE') }}
                              <a class="btn btn-sm btn-info" href="{{ route('customer.edit', $customer->id) }}">Edit</a>
                              <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                      			</form>
                    			</td>
        								</tr>
        							@endforeach
                		</tbody>
                	</table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
