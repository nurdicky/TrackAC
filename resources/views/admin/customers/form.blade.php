@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Add Data Customer
                	<a style="float: right;" class="btn btn-primary" href="{{ route('customer.index') }}">Back</a>
            	</div>

                <div class="panel-body">

                    @if(@$customers->exists) <?php  $url = route('customer.update', @$customers->id); ?>
                    @else <?php  $url = route('customer.store'); ?>
                    @endif
                	<form class="form-horizontal" action="{{ $url }}" method="POST">

                        @if (@$customers->exists)
                            {{ method_field('PUT') }}
                        @else
                            {{ method_field('POST') }}
                        @endif

                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('customer_name') ? ' has-error' : '' }}">
                            <label for="customer_name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="customer_name" type="text" class="form-control" name="customer_name" value="{{ @$customers->customer_name }}"required>

                                @if ($errors->has('customer_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('customer_address') ? ' has-error' : '' }}">
                            <label for="customer_address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <input id="customer_address" type="text" class="form-control" name="customer_address" value="{{ @$customers->customer_address }}"required>

                                @if ($errors->has('customer_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('customer_phone') ? ' has-error' : '' }}">
                            <label for="customer_phone" class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <input id="customer_phone" type="text" class="form-control" name="customer_phone" value="{{ @$customers->customer_phone }}"required>

                                @if ($errors->has('customer_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('customer_type') ? ' has-error' : '' }}">
                            <label for="customer_type" class="col-md-4 control-label">Customer Type</label>

                            <div class="col-md-6">
                                <input id="customer_type" type="text" class="form-control" name="customer_type" value="{{ @$customers->customer_type }}"required>

                                @if ($errors->has('customer_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @if (@$customers->exists)
                                        Update Customer
                                    @else
                                        Add Customer
                                    @endif
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
