@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Data Couriers
                	<a style="float: right;" class="btn btn-primary" href="{{ route('courier.create') }}">Add</a>
            	</div>

                <div class="panel-body">

                	@if(Session::has('alert-success'))
		                <div class="alert alert-success">
		                    <strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
		                </div>
		            @endif

                	<table class="table table-stripped table-responsive">
                		<thead>
                			<th>#</th>
                			<th>Name</th>
                			<th>Address</th>
                			<th>Phone</th>
                			<th>Courier Number</th>
                			<th></th>
                		</thead>
                		<tbody>
                			@php $no = 1; @endphp
                			@foreach ($couriers as $courier)
								<tr>
									<td>{{ $no++ }}</td>
									<td>{{ $courier->courier_name }}</td>
									<td>{{ $courier->courier_address }}</td>
									<td>{{ $courier->courier_phone }}</td>
									<td>{{ $courier->courier_number }}</td>
									<td>
										<form action="{{ route('courier.destroy', $courier->id) }}" method="post">
		                                {{ csrf_field() }}
		                                {{ method_field('DELETE') }}
		                                <a class="btn btn-sm btn-info" href="{{ route('courier.edit', $courier->id) }}">Edit</a>
		                                <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                            			</form>
                        			</td>
								</tr>
							@endforeach
                		</tbody>
                	</table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
