@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Add Data Couriers
                	<a style="float: right;" class="btn btn-primary" href="{{ route('courier.index') }}">Back</a>
            	</div>

                <div class="panel-body">

                    @if(@$couriers->exists) <?php  $url = route('courier.update', @$couriers->id); ?>
                    @else <?php  $url = route('courier.store'); ?>
                    @endif
                	<form class="form-horizontal" action="{{ $url }}" method="POST">

                        @if (@$couriers->exists)
                            {{ method_field('PUT') }}
                        @else
                            {{ method_field('POST') }}
                        @endif

                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('courier_name') ? ' has-error' : '' }}">
                            <label for="courier_name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="courier_name" type="text" class="form-control" name="courier_name" value="{{ @$couriers->courier_name }}" required>

                                @if ($errors->has('courier_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('courier_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('courier_address') ? ' has-error' : '' }}">
                            <label for="courier_address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <input id="courier_address" type="text" class="form-control" name="courier_address" value="{{ @$couriers->courier_address }}"required>

                                @if ($errors->has('courier_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('courier_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('courier_phone') ? ' has-error' : '' }}">
                            <label for="courier_phone" class="col-md-4 control-label">Phone Numbers</label>

                            <div class="col-md-6">
                                <input id="courier_phone" type="text" class="form-control" name="courier_phone" value="{{ @$couriers->courier_phone }}"required>

                                @if ($errors->has('courier_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('courier_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('courier_number') ? ' has-error' : '' }}">
                            <label for="courier_number" class="col-md-4 control-label">Courier Numbers</label>

                            <div class="col-md-6">
                                <input id="courier_number" type="text" class="form-control" name="courier_number" value="{{ @$couriers->courier_number }}"required>

                                @if ($errors->has('courier_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('courier_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('courier_password') ? ' has-error' : '' }}">
                            <label for="courier_password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="courier_password" type="password" class="form-control" name="courier_password" value="{{ @$couriers->courier_password }}"required>

                                @if ($errors->has('courier_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('courier_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @if (@$couriers->exists)
                                        Update Courier
                                    @else
                                        Add Courier
                                    @endif
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
