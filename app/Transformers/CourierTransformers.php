<?php

namespace App\Transformers;

use App\Models\Courier;
use League\Fractal\TransformerAbstract;

class CourierTransformers extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Courier $courier)
    {
        return [
            //
            'courier_name'        => $courier->courier_name,
            'courier_number'      => $courier->courier_number,
            'courier_address'     => $courier->courier_address,
            'courier_phone'       => $courier->courier_phone,
            'courier_registered'  => $courier->created_at->diffForHumans(),
        ];
    }
}
