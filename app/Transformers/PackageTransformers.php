<?php

namespace App\Transformers;

use App\Models\Package;
use League\Fractal\TransformerAbstract;

class PackageTransformers extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Package $package)
    {
        return [
            //
            'package_number'        => $package->package_number,
            'package_size'          => $package->package_size,
            'package_weight'        => $package->package_weight,
            'package_weight_unit'   => $package->package_weight_unit,
            'package_content'       => $package->package_content,
            'package_information'   => $package->package_information,
            'package_registered'    => $package->created_at->diffForHumans(),
        ];
    }
}
