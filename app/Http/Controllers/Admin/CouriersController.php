<?php

namespace App\Http\Controllers\Admin;

use App\Models\Courier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class CouriersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $couriers = Courier::all();
        return view('admin.couriers.list', ['couriers' => $couriers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.couriers.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $couriers = Courier::create($request->all());
        return redirect()->route('courier.index')->with('alert-success','Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $couriers = Courier::find($id);
        return view('admin.couriers.detail', ['couriers' => $couriers]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $couriers = Courier::find($id);
        return view('admin.couriers.form', ['couriers' => $couriers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $couriers = Courier::find($id);
        $couriers->courier_name = $request->courier_name;
        $couriers->courier_address = $request->courier_address;
        $couriers->courier_phone = $request->courier_phone;
        $couriers->courier_number = $request->courier_number;
        $couriers->courier_password = $request->courier_password;
        $couriers->save();

        return redirect()->route('courier.index')->with('alert-success','Berhasil Memperbarui Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $couriers = Courier::destroy($id);
        return redirect()->route('courier.index')->with('alert-success','Berhasil Menghapus Data!');
    }
}
