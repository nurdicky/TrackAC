<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class CustomerController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $customers = Customer::with('types')->get();
      return view('admin.customers.list', ['customers' => $customers]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      return view('admin.customers.form');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $customers = Customer::create($request->all());
      return redirect()->route('customer.index')->with('alert-success','Berhasil Menambahkan Data!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      $customers = Customer::find($id);
      return view('admin.customers.detail', ['customers' => $customers]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      $customers = Customer::find($id);
      return view('admin.customers.form', ['customers' => $customers]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      $customers = Customer::find($id);
      $customers->customer_name = $request->customer_name;
      $customers->customer_address = $request->customer_address;
      $customers->customer_phone = $request->customer_phone;
      $customers->customer_type = $request->customer_type;
      $customers->save();

      return redirect()->route('customer.index')->with('alert-success','Berhasil Memperbarui Data!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      $customers = Customer::destroy($id);
      return redirect()->route('customer.index')->with('alert-success','Berhasil Menghapus Data!');
  }
}
