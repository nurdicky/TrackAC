<?php

namespace App\Http\Controllers\Admin;

use App\Models\ShipmentChekpoint;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class ShipmentChekpointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $checkpoints = ShipmentChekpoint::all();
        return view('admin.shipments.checkpoints.list', ['checkpoints' => $checkpoints]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.shipments.checkpoints.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $checkpoints = ShipmentChekpoint::create($request->all());
        return redirect()->route('checkpoint.index')->with('alert-success','Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $checkpoints = ShipmentChekpoint::find($id);
        return view('admin.shipments.checkpoints.detail', ['checkpoints' => $checkpoints]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $checkpoints = ShipmentChekpoint::find($id);
        return view('admin.shipments.checkpoints.form', ['checkpoints' => $checkpoints]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $checkpoints = ShipmentChekpoint::find($id);
        $checkpoints->package_id = $request->package_id;
        $checkpoints->checkpoint_time = $request->checkpoint_time;
        $checkpoints->checkpoint_cordinate = $request->checkpoint_cordinate;
        $checkpoints->checkpoint_status = $request->checkpoint_status;
        $checkpoints->save();

        return redirect()->route('checkpoint.index')->with('alert-success','Berhasil Memperbarui Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checkpoints = ShipmentChekpoint::destroy($id);
        return redirect()->route('checkpoint.index')->with('alert-success','Berhasil Menghapus Data!');
    }
}
