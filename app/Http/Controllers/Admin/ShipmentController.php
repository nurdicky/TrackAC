<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\Shipment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class ShipmentController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $shipments = Shipment::with(['shippers', 'consignees', 'couriers', 'services'])->get();

      return view('admin.shipments.list', ['shipments' => $shipments]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      return view('admin.shipments.form');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $shipments = Shipment::create($request->all());
      return redirect()->route('shipment.index')->with('alert-success','Berhasil Menambahkan Data!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      $shipments = Shipment::find($id);
      return view('admin.shipments.detail', ['shipments' => $shipments]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      $shipments = Shipment::find($id);
      return view('admin.shipments.form', ['shipments' => $shipments]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      $shipments = Shipment::find($id);
      $shipments->shipment_shipper = $request->shipment_shipper;
      $shipments->shipment_consignee = $request->shipment_consignee;
      $shipments->shipment_courier = $request->shipment_courier;
      $shipments->shipment_origin = $request->shipment_origin;
      $shipments->shipment_destination = $request->shipment_destination;
      $shipments->shipment_date = $request->shipment_date;
      $shipments->shipment_expected_delivery = $request->shipment_expected_delivery;
      $shipments->shipment_service = $request->shipment_service;
      $shipments->shipment_package_count = $request->shipment_package_count;
      $shipments->save();

      return redirect()->route('shipment.index')->with('alert-success','Berhasil Memperbarui Data!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      $shipments = Shipment::destroy($id);
      return redirect()->route('shipment.index')->with('alert-success','Berhasil Menghapus Data!');
  }
}
