<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Transformers\UserTransformers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{
    //
    public function users()
    {
      $users = User::all();
      return fractal()
            ->collection($users)
            ->transformWith( new UserTransformers)
            ->toArray();
    }
}
