<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipmentChekpoint extends Model
{
    //
    protected $fillable = [
        'package_id',
        'checkpoint_time',
        'checkpoint_cordinate',
        'checkpoint_status',
    ];

    public function statuses()
    {
        return $this->hasOne('App\Models\ShipmentStatus', 'checkpoint_status');
    }

    public function packages()
    {
        return $this->hasOne('App\Models\Package', 'package_id');
    }


}
