<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipmentPackageList extends Model
{
    //
    protected $fillable = [
        'package_id',
        'shipment_id',
    ];

    public function packages()
    {
        return $this->hasMany('App\Models\Package', 'package_id');
    }

    public function shipments()
    {
        return $this->hasOne('App\Models\Shipment', 'shipment_id');
    }
}
