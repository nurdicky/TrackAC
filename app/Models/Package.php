<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    //
    protected $fillable = [
        'package_number',
        'package_size',
        'package_weight',
        'package_weight_unit',
        'package_content',
        'package_information',
    ];

    public function packageLists()
    {
        return $this->belongsTo('App\Models\ShipmentPackageList', 'package_id');
    }

    public function checkpoints()
    {
        return $this->belongsTo('App\Models\ShipmentChekpoint', 'package_id');
    }
}
