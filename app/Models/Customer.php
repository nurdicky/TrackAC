<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $fillable = [
        'customer_name',
        'customer_address',
        'customer_phone',
        'customer_type',
    ];

    public function types()
    {
        return $this->belongsTo('App\Models\CustomerType', 'customer_type');
    }

    public function shipmentShippers()
    {
        return $this->hasMany('App\Models\Shipment', 'shipment_shipper');
    }

    public function shipmentConsignees()
    {
        return $this->hasMany('App\Models\Shipment', 'shipment_consignee');
    }

}
