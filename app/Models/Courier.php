<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Courier extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'courier_name',
        'courier_address',
        'courier_phone',
        'courier_number',
        'courier_password',
        'api_token',
    ];

    protected $hidden = [
        'api_token',
    ];


    public function shipments()
    {
        return $this->belongsTo('App\Models\Shipment', 'shipment_courier');
    }

}
