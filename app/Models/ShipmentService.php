<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipmentService extends Model
{
    //
    protected $fillable = [
        'service_name',
        'service_type',
    ];

    public function shipments()
    {
        return $this->belongsTo('App\Models\Shipment', 'shipment_service');
    }
}
