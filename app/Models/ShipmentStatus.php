<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipmentStatus extends Model
{
    //
    protected $fillable = [
        'status_tag',
        'status_message',
    ];

    public function checkpoints()
    {
        return $this->belongsTo('App\Models\ShipmentChekpoint', 'checkpoint_status');
    }
}
