<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerType extends Model
{
    //
    protected $fillable = [
        'customer_type_name',
    ];

    public function customers()
    {
        return $this->hasMany('App\Models\Customer', 'customer_type');
    }
}
