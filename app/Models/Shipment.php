<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    //
    protected $fillable = [
        'shipment_shipper',
        'shipment_consignee',
        'shipment_courier',
        'shipment_origin',
        'shipment_destination',
        'shipment_date',
        'shipment_expected_delivery',
        'shipment_service',
        'shipment_package_count',
    ];

    public function shippers()
    {
        return $this->belongsTo('App\Models\Customer', 'shipment_shipper')->where('customer_type', 1);
    }

    public function consignees()
    {
        return $this->belongsTo('App\Models\Customer', 'shipment_consignee')->where('customer_type', 2);
    }

    public function couriers()
    {
        return $this->hasOne('App\Models\Courier', 'id');
    }

    public function services()
    {
        return $this->hasOne('App\Models\ShipmentService', 'id');
    }

    public function shipmentLists()
    {
      return $this->belongsTo('App\Models\ShipmentPackageList', 'package_id');
    }
}
